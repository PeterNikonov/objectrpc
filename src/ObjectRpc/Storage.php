<?php

namespace ObjectRpc;

/**
 * Хранение состояния объектов между запросами
 * @author Dell
 */
class Storage {

    protected $sessionKey;

    public function __construct($sessionKey = 'ChO') {

        $this->sessionKey = $sessionKey;
    }

    /**
     * @param ObjectRpc $object
     */
    public function setInstance( ObjectRpc $object) {
        $_SESSION[$this->sessionKey] = serialize($object);
    }

    /**
     * 
     * @return ObjectRpc
     */
    public function getInstance() {
        $object = (isset($_SESSION[$this->sessionKey])) ? unserialize($_SESSION[$this->sessionKey]) : new ObjectRpc;
        return $object;
    }

    public function dropInstance() {
        if(isset($_SESSION[$this->sessionKey])) { unset($_SESSION[$this->sessionKey]); }
    }

}
