
$.ajaxSetup({async: false});
var positionRpcUrl = "/warehouse/rpc/position.php";

function test(response) {
    if (response.result) {
        console.log(response.result);
    }
}


function JsonRequest(url, request, callback) {
    $.post(url, JSON.stringify(request), callback, "json");
}

function addValue(property, value) {

    var request = {}

    request.method = "addValue";
    request.params = [property, value];
    request.id = 1;
    request.jsonrpc = "2.0";

    response = JsonRequest(positionRpcUrl, request, test);

}

function addItem(property, value) {

    var request = {}

    request.method = "addItem";
    request.params = [property, value];
    request.id = 1;
    request.jsonrpc = "2.0";

    response = JsonRequest(positionRpcUrl, request, test);

}

function removeItem(property, value) {

    var request = {}

    request.method = "removeItem";
    request.params = [property, value];
    request.id = 1;
    request.jsonrpc = "2.0";

    response = JsonRequest(positionRpcUrl, request, test);

}

function setAssocItem(property, key, $value) {

    var request = {}

    request.method = "setAssocItem";
    request.params = [property, key, $value];

    request.id = 3;
    request.jsonrpc = "2.0";
    console.log(request.method);
    response = JsonRequest(positionRpcUrl, request, test);
}

