<?php

session_start();
include '../../vendor/autoload.php';

use JsonRPC\Server;
use ObjectRpc\Procedure;
use ObjectRpc\Storage;

$storage = new Storage('position');
$server = new Server();

// динамическое изменение свойств объекта
$server->getProcedureHandler()->withObject(new Procedure($storage));

echo $server->execute();
