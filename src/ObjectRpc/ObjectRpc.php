<?php

namespace ObjectRpc;

/**
 * Реализует стандартные методы для динамического изменения свойств хранимого объекта
 * Генерирует объекты с нужными свойствами
 */
class ObjectRpc {

    /** устанавливает значение для простого свойства объекта */
    public function addValue($property, $value) {
        $this->$property = $value;
        return $this;
    }

    /** удаляет значение для простого свойства объекта */
    public function removeValue($property) {
        $this->$property = null;
        return $this;
    }

    /** добавить значение в неассоциированный массив свойства объекта */
    public function addItem($property, $value) {
        $array = $this->$property;
        if (@!in_array($value, $array)) {
            $array[] = $value;
            $this->$property = $array;
        }
        return $this;
    }

    /** удалить значение неассоцииорованного массива свойства объекта */
    public function removeItem($property, $value) {
        $array = $this->$property;
        if ($array) {
            foreach ($array as $key => $item) {
                if ($item !== $value)
                    continue;
                unset($array[$key]);
            }
            $this->$property = $array;
        }
        return $this;
    }

    /** добавляет ассоциированное значение в массив свойства объекта */
    public function addAssocItem($property, $assoc, $value) {
        $array = $this->$property;
        $array[$assoc] = $value;
        $this->$property = $array;
        return $this;
    }

    /** удаляет ассоциированное значение из массива свойства объекта */
    public function removeAssocItem($property, $key) {
        $array = $this->$property;
        unset($array[$key]);
        $this->$property = $array;
        return $this;
    }

}
