<?php

namespace ObjectRpc;

/**
 * Обрабатывает запросы на изменения свойств объекта
 * @author Dell
 */
class Procedure {

    /**
     *
     * @var Storage
     */
    protected $storage;

    public function __construct(Storage $storage) {
        $this->storage = $storage;
    }

    public function addValue($property, $value) {
        $this->storage->setInstance($this->storage->getInstance()->addValue($property, $value));
    }

    public function addItem($property, $value) {
        $this->storage->setInstance($this->storage->getInstance()->addItem($property, $value));
    }

    public function removeItem($property, $value) {
        $this->storage->setInstance($this->storage->getInstance()->removeItem($property, $value));
    }

    public function setAssocItem($property, $assoc, $value) {
        $this->storage->setInstance($this->storage->getInstance()->addAssocItem($property, $assoc, $value));
    }

    public function removeAssocItem($property, $assoc, $value) {
        $this->storage->setInstance($this->storage->getInstance()->removeAssocItem($property, $assoc, $value));
    }

    public function removeProperty($property) {
        $this->storage->setInstance($this->storage->getInstance()->removeValue($property));
    }

}
